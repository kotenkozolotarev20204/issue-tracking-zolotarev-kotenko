#pragma once
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class FileS
{
	const string path = "Computers.txt";
	const string pathOrder = "Order.txt";
	const string copyPath = "Order1.txt";
	unsigned int fileSize;

public:

	struct computers
	{
		string id;
		string name;
		struct components
		{
			string gpu;
			string cpu;
			string motherboard;
			string ram;
			string pw;
		} component;
		double cost;
	};

	void createFile(string _id, string _name, string _gpu, string _cpu, string _motherboard, string _ram, string _pw, double _cost)
	{
		ofstream inFile(path);

		inFile << _id << ' ';
		inFile << _name << ' ';
		inFile << _gpu << ' ';
		inFile << _cpu << ' ';
		inFile << _motherboard << ' ';
		inFile << _ram << ' ';
		inFile << _pw << ' ';
		inFile << _cost << endl;

		inFile.close();
	}

	void addComputer(string _id, string _name, string _gpu, string _cpu, string _motherboard, string _ram, string _pw, double _cost)
	{
		ofstream inFile(path, ios::app);

		inFile << _id << ' ';
		inFile << _name << ' ';
		inFile << _gpu << ' ';
		inFile << _cpu << ' ';
		inFile << _motherboard << ' ';
		inFile << _ram << ' ';
		inFile << _pw << ' ';
		inFile << _cost << endl;

		inFile.close();
	}

	int printComputerList()
	{
		ifstream fromFile(path);
		
		try
		{
			if (!fromFile)
			{
				throw "�� ������� ������� ����. ��������, ���������� ��� �������!";
			}

			unsigned int counter{ 1 };
			fileSize = 0;
			string buf;

			while (true)
			{
				getline(fromFile, buf);

				if (fromFile.eof())
				{
					break;
				}

				cout << buf;
				counter++;
				fileSize++;
			}

			fromFile.close();
			return 0;
		}
		catch (const char* error)
		{
			cerr << error << endl;
			return 1;
		}
	}

	computers findComputer(unsigned int computerNum)
	{
		ifstream fromFile(path);

		try
		{
			if (!fromFile)
			{
				throw "�� ������� ������� ����. ��������, ���������� ��� �������!";
			}

			computers foundedComputer{};
			unsigned int counter{ 1 };
			string buf;
			while (true)
			{
				if (counter == computerNum)
				{
					break;
				}
				getline(fromFile, buf);
				counter++;
			}

			fromFile >> foundedComputer.id;
			fromFile >> foundedComputer.name;
			fromFile >> foundedComputer.component.gpu;
			fromFile >> foundedComputer.component.cpu;
			fromFile >> foundedComputer.component.motherboard;
			fromFile >> foundedComputer.component.ram;
			fromFile >> foundedComputer.component.pw;
			fromFile >> foundedComputer.cost;

			fromFile.close();

			return foundedComputer;
		}
		catch (const char* error)
		{
			cerr << error << endl;
		}
	}

	void deleteComputer(unsigned int computerNum)
	{
		unsigned int counter{ 1 };
		computers deletedComputer;

		deletedComputer = findComputer(computerNum);

		fstream file(path, ios::in | ios::out);

		string* buf;
		buf = new string[fileSize];
		unsigned int i{};

		while ((i + 1) != fileSize)
		{
			string copy;
			file >> copy;
			if (copy == deletedComputer.id)
			{
				string cache;
				getline(file, cache);
				continue;
			}
			int len = copy.length();
			file.seekg(-len, ios::cur);
			file.seekp(file.tellp(), ios::beg);

			getline(file, buf[i]);
			i++;
		}
		file.close();

		ofstream inFile(path);

		for (int j{}; j < i; j++)
		{
			inFile << buf[j] << endl;
		}

		inFile.close();
		delete[] buf;
	}

	void createOrderFile()
	{
		ofstream inFile(pathOrder);
		inFile.close();
	}

	void addOrder(string _name, string _surname, string _idComputer)
	{
		ifstream readFile(pathOrder, ios::in);
		ofstream inFile(pathOrder, ios::app);

		try
		{
			if (!inFile)
			{
				throw "�� ������� ������� ����.��������, ���������� ��� �������!";
			}

			int maxId = 0;

			while (true)
			{
				if (readFile.eof())
				{
					break;
				}

				readFile >> maxId;
				string name, surname;
				int idComputer;
				readFile >> name;
				readFile >> surname;
				readFile >> idComputer;
			}

			inFile << maxId + 1 << ' ';
			inFile << _name << ' ';
			inFile << _surname << ' ';
			inFile << _idComputer << endl;

			readFile.close();
			inFile.close();
		}

		catch (const char* error)
		{
			cerr << error << endl;
		}
	}

	void searchingOrder(string _surname, string _name)
	{
		ifstream fromFile(pathOrder);

		try
		{
			if (!fromFile)
			{
				throw "�� ������� ������� ����. ��������, ���������� ��� �������!";
			}
			struct Orders
			{
				int id;
				string name;
				string surname;
				int idComputers;
			} orderComputer{};

			while (true)
			{
				fromFile >> orderComputer.id;
				fromFile >> orderComputer.name;
				fromFile >> orderComputer.surname;
				fromFile >> orderComputer.idComputers;

				if (fromFile.eof())
				{
					break;
				}

				if ((orderComputer.name == _name) && (orderComputer.surname == _surname))
				{
					cout << orderComputer.id << ' ';
					cout << orderComputer.name << ' ';
					cout << orderComputer.surname << ' ';
					cout << orderComputer.idComputers << endl;
				}
			}

			fromFile.close();
		}

		catch (const char* error)
		{
			cerr << error << endl;
		}
	}

	void deleteOrder(int id)
	{
		ifstream fromFile(pathOrder);
		ofstream inFile(copyPath);
		if (!fromFile)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����" << endl;
		}
		else
		{
			struct Orders
			{
				int id;
				string name;
				string surname;
				int idComputers;
			} orderComputer{};

			while (true)
			{
				fromFile >> orderComputer.id;
				fromFile >> orderComputer.name;
				fromFile >> orderComputer.surname;
				fromFile >> orderComputer.idComputers;

				if (fromFile.eof())
				{
					break;
				}

				if (orderComputer.id != id)
				{
					inFile << orderComputer.id << ' ';
					inFile << orderComputer.name << ' ';
					inFile<< orderComputer.surname << ' ';
					inFile << orderComputer.idComputers << endl;
				}
			}

			inFile.close();
			fromFile.close();

			remove("Order.txt");
			rename("Order1.txt", "Order.txt");
		}
	}
};