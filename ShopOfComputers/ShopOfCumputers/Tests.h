#pragma once
#include <iostream>
#include "File.h"

void test(FileS::computers testComputer)
{
	try
	{
		if (testComputer.id == "")
		{
			throw "������ ���� id. ���������� ������� ������ ����.";
		}
		if (testComputer.name == "")
		{
			throw "������ ���� name. ���������� ������� ������ ����.";
		}
		if (testComputer.component.cpu == "")
		{
			throw "������ ���� cpu. ���������� ������� ������ ����.";
		}
		if (testComputer.component.gpu == "")
		{
			throw "������ ���� gpu. ���������� ������� ������ ����.";
		}
		if (testComputer.component.motherboard == "")
		{
			throw "������ ���� motherboard. ���������� ������� ������ ����.";
		}
		if (testComputer.component.ram == "")
		{
			throw "������ ���� ram. ���������� ������� ������ ����.";
		}
		if (testComputer.component.pw == "")
		{
			throw "������ ���� pw. ���������� ������� ������ ����.";
		}
	}
	catch(const char* error)
	{
		cerr << error << endl;
		system("pause");
	}
}
