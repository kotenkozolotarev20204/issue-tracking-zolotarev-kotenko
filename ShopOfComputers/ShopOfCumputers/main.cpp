﻿#include <iostream>
#include "Computer.h"
#include "Selling.h"
#include "Tests.h"

using namespace std;

int main()
{
    setlocale(0, "");
    int ch, key;
    cout << "Вы кто?" << endl;
    cout << "1. Продавец" << endl;
    cout << "2. Покупатель" << endl;
    cin >> key;
    Selling sell(0, "", "", "");
    sell.CreateFile();
    do
    {
        system("cls");
        if (key == 1)
        {
            cout << "Меню" << endl;
            cout << "1. Создание файла для хранения информации о компьютерах" << endl;
            cout << "2. Закупка компьютера" << endl;
            cout << "3. Список всех компьютеров" << endl;
            cout << "4. Изменить характеристики компьютера" << endl;
            cout << "5. Выход" << endl;
            cout << "Введите номер действия: ";
            cin >> ch;
            switch (ch)
            {
            case 1:
            {
                FileS computersFile;
                FileS::computers firstComputer;

                system("cls");
                cout << "Введите id первого компьютера: ";
                cin.ignore();
                getline(cin, firstComputer.id);
                cout << "Введите название производителя компьютера: ";
                getline(cin, firstComputer.name);
                cout << "Введите видеокарту: ";
                getline(cin, firstComputer.component.gpu);
                cout << "Введите процессор: ";
                getline(cin, firstComputer.component.cpu);
                cout << "Введите материнскую плату: ";
                getline(cin, firstComputer.component.motherboard);
                cout << "Введите оперативную память: ";
                getline(cin, firstComputer.component.ram);
                cout << "Введите блок питания: ";
                getline(cin, firstComputer.component.pw);
                cout << "Введите стоимость: ";
                cin >> firstComputer.cost;

                test(firstComputer);

                computersFile.createFile(firstComputer.id, firstComputer.name, firstComputer.component.gpu, firstComputer.component.cpu, firstComputer.component.motherboard, firstComputer.component.ram, firstComputer.component.pw, firstComputer.cost);
            }
            break;
            case 2:
            {
                FileS::computers newComputer;
              
                system("cls");
                cout << "Введите id нового компьютера: ";
                cin.ignore();
                getline(cin, newComputer.id);
                cout << "Введите название производителя компьютера: ";
                getline(cin, newComputer.name);
                cout << "Введите видеокарту: ";
                getline(cin, newComputer.component.gpu);
                cout << "Введите процессор: ";
                getline(cin, newComputer.component.cpu);
                cout << "Введите материнскую плату: ";
                getline(cin, newComputer.component.motherboard);
                cout << "Введите оперативную память: ";
                getline(cin, newComputer.component.ram);
                cout << "Введите блок питания: ";
                getline(cin, newComputer.component.pw);
                cout << "Введите стоимость: ";
                cin >> newComputer.cost;

                test(newComputer);

                Computer boughtComputer(newComputer.id, newComputer.name, newComputer.component.gpu, newComputer.component.cpu, newComputer.component.motherboard, newComputer.component.ram, newComputer.component.pw, newComputer.cost);

                boughtComputer.buyComputer();
            }
            break;
            case 3:
            {
                Computer computersList;
                system("cls");
                computersList.printComputers();
                system("pause");
            }
            break;
            case 4:
            {
                system("cls");
                Computer changeComputer;
                changeComputer.changeSpecifications();
            }
            break;
            case 5:
            {
                exit(0);
            }
            break;
            }
        }
        else
        {
            cout << "Меню" << endl;
            cout << "1. Заказать компьютер" << endl;
            cout << "2. Оплатить компьютер" << endl;
            cout << "3. Вернуть компьютер" << endl;
            cout << "4. Выход" << endl;
            cout << "Введите номер действия: ";
            cin >> ch;
            
            switch (ch)
            {
            case 1:
            {
                system("cls");
                sell.orderComputer();
            }
            break;
            case 2:
            {
                system("cls");
                sell.payOrder();
            }
            break;
            case 3:
            {
                system("cls");
                sell.returnComputer();
            }
            break;
            case 4:
            {
                exit(0);
            }
            break;
            }
        }
    } while (true);

    return 0;
}