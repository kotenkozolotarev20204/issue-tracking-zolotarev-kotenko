#pragma once
#include <string>
#include <iostream>
#include "File.h"

using namespace std;

class Computer
{
public:

	string id;
	string name;
	string gpu;
	string cpu;
	string motherboard;
	string ram;
	string pw;
	double cost;

	Computer(string _id, string _name, string _gpu, string _cpu, string _motherboard, string _ram, string _pw, double _cost)
	{
		id = _id;
		name = _name;
		gpu = _gpu;
		cpu = _cpu;
		motherboard = _motherboard;
		ram = _ram;
		pw = _pw;
		cost = _cost;
	}

	Computer()
	{}

	void buyComputer()
	{
		FileS computersFile;
		computersFile.addComputer(id, name, gpu, cpu, motherboard, ram, pw, cost);
	}

	void printComputers()
	{
		FileS computersFile;
		computersFile.printComputerList();
	}

	void changeSpecifications()
	{
		FileS computersFile;
		try
		{
			if (computersFile.printComputerList())
			{
				throw "��-�� ������ �������� ����� ���������� ������� �����������.";
			}

			cout << endl;
			unsigned int computerNumber, counter{ 1 };
			cout << "������� ����� ���������� � ������ ��� ��������� �������������: ";
			cin >> computerNumber;
			system("cls");

			FileS::computers changedComputer;
			changedComputer = computersFile.findComputer(computerNumber);

			int ch;
			bool check{};
			do
			{
				system("cls");
				cout << "1. �������� id ����������." << endl;
				cout << "2. �������� ��� ������������� ����������." << endl;
				cout << "3. �������� ���������� ����������." << endl;
				cout << "4. �������� ��������� ����������." << endl;
				cout << "5. �������� ����������� ����� ����������." << endl;
				cout << "6. �������� ����������� ������ ����������." << endl;
				cout << "7. �������� ���� ������� ����������." << endl;
				cout << "8. �������� ���� ����������." << endl;
				cout << "9. �����." << endl;
				cout << "������� ����� ��������: ";
				cin >> ch;
				switch (ch)
				{
				case 1:
				{
					cout << "������� ����� id: ";
					getline(cin, changedComputer.id);
				}
				break;
				case 2:
				{
					cout << "������� ����� ��� �������������: ";
					getline(cin, changedComputer.name);
				}
				break;
				case 3:
				{
					cout << "������� ����� ����������: ";
					getline(cin, changedComputer.component.gpu);
				}
				break;
				case 4:
				{
					cout << "������� ����� ���������: ";
					getline(cin, changedComputer.component.cpu);
				}
				break;
				case 5:
				{
					cout << "������� ����� ����������� �����: ";
					getline(cin, changedComputer.component.motherboard);
				}
				break;
				case 6:
				{
					cout << "������� ����� ����������� ������: ";
					getline(cin, changedComputer.component.ram);
				}
				break;
				case 7:
				{
					cout << "������� ����� ���� �������: ";
					getline(cin, changedComputer.component.pw);
				}
				break;
				case 8:
				{
					cout << "������� ����� ����: ";
					cin >> changedComputer.cost;
				}
				break;
				case 9:
				{
					check = 1;
				}
				}
			} while (check != 1);

			computersFile.deleteComputer(computerNumber);
			computersFile.addComputer(changedComputer.id, changedComputer.name, changedComputer.component.gpu, changedComputer.component.cpu, changedComputer.component.motherboard, changedComputer.component.ram, changedComputer.component.pw, changedComputer.cost);
		}
		catch (const char* error)
		{
			cerr << error << endl;
			system("pause");
		}

	}
};