#pragma once
#include "File.h"

class Selling
{
public:
	int id;
	string surname, name, idComputer = "";
	FileS file;

	Selling(int _id, string _name, string _surname, string _idComputer)
	{
		id = _id;
		name = _name;
		surname = _surname;
		idComputer = _idComputer;
	}

	void CreateFile()
	{
		file.createOrderFile();
	}

	void orderComputer()
	{
		try
		{
			if (file.printComputerList())
			{
				throw "������ ������ ������! �� ���������� ������ �����������!";
			}

			cout << "������� id ������, ������� ������ ��������: ";
			cin >> idComputer;
			system("pause");
		}

		catch (const char* error)
		{
			cerr << error << endl;
			system("pause");
		}
	}

	void payOrder()
	{
		try
		{
			if (file.printComputerList())
			{
				throw "������ ������� ������! �� ���������� ������ �����������!";
			}

			if (idComputer[0] == '\0')
			{
				throw "������ ������� ������! ����� �� �������!";
			}

			cout << "��� ����� � ��������������� " << idComputer << ". �� ������� ��� ������ ��� ����������? 1 - ��; 2 - ���" << endl;
			int key = 0;
			cin >> key;
			if (key == 1)
			{
				cout << "������� ���� �������: ";
				cin >> surname;
				cout << "������� ��� ���: ";
				cin >> name;
				file.addOrder(name, surname, idComputer);
				cout << "��� ����� �������. ������ ��� ��������� ������." << endl;
			}
			else
			{
				cout << "��� ����� �� �������. �� ������ ������� ������ ���������." << endl;
			}
			system("pause");
		}
		catch (const char* error)
		{
			cerr << error << endl;
			system("pause");
		}
	}

	void returnComputer()
	{
		try
		{
			if (file.printComputerList())
			{
				throw "������ �������� ������! �� ���������� ������ �����������!";
			}

			if (idComputer[0] == '\0')
			{
				throw "������ �������� ������! ����� �� ��� �������!";
			}

			cout << "������� ���� �������: ";
			cin >> surname;
			cout << "������� ��� ���: ";
			cin >> name;
			file.searchingOrder(surname, name);
			cout << "������� id ������: ";
			cin >> id;
			file.deleteOrder(id);
			cout << "��� ����� ���������.";
			system("pause");
		}
		
		catch (const char* error)
		{
			cerr << error << endl;
			system("pause");
		}
	}
};