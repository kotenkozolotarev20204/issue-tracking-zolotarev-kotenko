#pragma once
#include "BuyComputer.h"
#include "ChangeSpecifications2.h"
#include "CreateComputerFile.h"
#include "PrintComputers.h"

namespace Project1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� SellerMenu
	/// </summary>
	public ref class SellerMenu : public System::Windows::Forms::Form
	{
	public:
		SellerMenu(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~SellerMenu()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	protected:
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button4;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button1;

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(82, 11);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(45, 17);
			this->label1->TabIndex = 11;
			this->label1->Text = L"����";
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(12, 147);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(197, 23);
			this->button5->TabIndex = 10;
			this->button5->Text = L"�����";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &SellerMenu::button5_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(12, 118);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(197, 23);
			this->button4->TabIndex = 9;
			this->button4->Text = L"�������� ��������������";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &SellerMenu::button4_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(12, 89);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(197, 23);
			this->button3->TabIndex = 8;
			this->button3->Text = L"������ ���� �����������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &SellerMenu::button3_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(12, 60);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(197, 23);
			this->button2->TabIndex = 7;
			this->button2->Text = L"������� ����������\r\n";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &SellerMenu::button2_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 31);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(197, 23);
			this->button1->TabIndex = 6;
			this->button1->Text = L"������� ����\r\n";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &SellerMenu::button1_Click);
			// 
			// SellerMenu
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(226, 185);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Name = L"SellerMenu";
			this->Text = L"SellerMenu";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		CreateComputerFile form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		BuyComputer form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		PrintComputers form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		ChangeSpecifications2 form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Close();
	}
};
}
