#pragma once
#include <string>
#include <iostream>
#include "File.h"

using namespace std;

class Computer
{
public:

	string id;
	string name;
	string gpu;
	string cpu;
	string motherboard;
	string ram;
	string pw;
	double cost;

	Computer(string _id, string _name, string _gpu, string _cpu, string _motherboard, string _ram, string _pw, double _cost)
	{
		id = _id;
		name = _name;
		gpu = _gpu;
		cpu = _cpu;
		motherboard = _motherboard;
		ram = _ram;
		pw = _pw;
		cost = _cost;
	}

	Computer()
	{}

	void buyComputer()
	{
		FileS computersFile;
		computersFile.addComputer(id, name, gpu, cpu, motherboard, ram, pw, cost);
	}

	void changeSpecifications(FileS::computers newData, unsigned int compNumber)
	{
		FileS computersFile;
		unsigned int counter{ 1 };

		FileS::computers changedComputer;
		changedComputer = computersFile.findComputer(compNumber);

		changedComputer.id = newData.id;
		changedComputer.name = newData.name;
		changedComputer.component.gpu = newData.component.gpu;
		changedComputer.component.cpu = newData.component.cpu;
		changedComputer.component.motherboard = newData.component.motherboard;
		changedComputer.component.ram = newData.component.ram;
		changedComputer.component.pw = newData.component.pw;
		changedComputer.cost = newData.cost;

		computersFile.deleteComputer(compNumber);
		computersFile.addComputer(changedComputer.id, changedComputer.name, changedComputer.component.gpu, changedComputer.component.cpu, changedComputer.component.motherboard, changedComputer.component.ram, changedComputer.component.pw, changedComputer.cost);
	}
};