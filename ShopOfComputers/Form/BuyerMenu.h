#pragma once
#include "PayOrder.h"
#include "ReturnComputer.h"
#include "Selling.h"

namespace Project1 {

	using namespace System;
	using namespace std;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� BuyerMenu
	/// </summary>
	public ref class BuyerMenu : public System::Windows::Forms::Form
	{
	public:
		BuyerMenu(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~BuyerMenu()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^ label1;
	protected:
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button2;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(88, 15);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(45, 17);
			this->label1->TabIndex = 16;
			this->label1->Text = L"����";
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(22, 93);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(191, 23);
			this->button5->TabIndex = 15;
			this->button5->Text = L"�����";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &BuyerMenu::button5_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(22, 64);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(191, 23);
			this->button3->TabIndex = 14;
			this->button3->Text = L"������� ���������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &BuyerMenu::button3_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(22, 35);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(191, 23);
			this->button2->TabIndex = 13;
			this->button2->Text = L"�������� ���������";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &BuyerMenu::button2_Click);
			// 
			// BuyerMenu
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(244, 128);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Name = L"BuyerMenu";
			this->Text = L"BuyerMenu";
			this->Load += gcnew System::EventHandler(this, &BuyerMenu::BuyerMenu_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		   
#pragma endregion
	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		PayOrder form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) 
	{
		ReturnComputer form;
		form.Owner = this;
		this->Hide();
		form.ShowDialog();
	}
	private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e)
	{
		Close();
	}
	private: System::Void BuyerMenu_Load(System::Object^ sender, System::EventArgs^ e) 
	{
		Selling sell;
		sell.CreateOrderFile();
	}
};
}
