#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <io.h>
#include <msclr/marshal_cppstd.h>

using namespace std;

class FileS
{
	const string path = "Computers.txt";
	const string pathOrder = "Order.txt";
	const string copyPath = "Order1.txt";
	unsigned int fileSize;

public:

	struct computers
	{
		string id;
		string name;
		struct components
		{
			string gpu;
			string cpu;
			string motherboard;
			string ram;
			string pw;
		} component;
		double cost;
	};

	void createFile(string _id, string _name, string _gpu, string _cpu, string _motherboard, string _ram, string _pw, double _cost)
	{
		ofstream inFile(path);

		inFile << _id << ' ';
		inFile << _name << ' ';
		inFile << _gpu << ' ';
		inFile << _cpu << ' ';
		inFile << _motherboard << ' ';
		inFile << _ram << ' ';
		inFile << _pw << ' ';
		inFile << _cost << endl;

		inFile.close();
	}

	void addComputer(string _id, string _name, string _gpu, string _cpu, string _motherboard, string _ram, string _pw, double _cost)
	{
		ofstream inFile(path, ios::app);

		if (!inFile)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����";
		}
		else
		{
			inFile << _id << ' ';
			inFile << _name << ' ';
			inFile << _gpu << ' ';
			inFile << _cpu << ' ';
			inFile << _motherboard << ' ';
			inFile << _ram << ' ';
			inFile << _pw << ' ';
			inFile << _cost << endl;

			inFile.close();
		}
	}

	void printComputerList(System::Windows::Forms::TextBox^ textbox)
	{
		ifstream fromFile(path);
		string buf;
		while (true)
		{
			getline(fromFile, buf);

			if (fromFile.eof())
			{
				break;
			}
			buf += "\n";
			textbox->AppendText(msclr::interop::marshal_as<System::String^>(buf));
		}
	}

	computers findComputer(unsigned int computerNum)
	{
		ifstream fromFile(path);

		if (!fromFile)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����";
		}
		else
		{
			computers foundedComputer{};
			unsigned int counter{ 1 };
			string buf;
			while (true)
			{
				if (counter == computerNum)
				{
					break;
				}
				getline(fromFile, buf);
				counter++;
			}

			fromFile >> foundedComputer.id;
			fromFile >> foundedComputer.name;
			fromFile >> foundedComputer.component.gpu;
			fromFile >> foundedComputer.component.cpu;
			fromFile >> foundedComputer.component.motherboard;
			fromFile >> foundedComputer.component.ram;
			fromFile >> foundedComputer.component.pw;
			fromFile >> foundedComputer.cost;

			fromFile.close();

			return foundedComputer;
		}
	}

	void deleteComputer(unsigned int computerNum)
	{
		unsigned int counter{ 1 };
		computers deletedComputer;

		deletedComputer = findComputer(computerNum);

		ifstream fromFile(path);
		string copy;
		int fileSize = 0;

		while (getline(fromFile, copy))
		{
			fileSize++;
		}
		fromFile.close();

		fstream file(path, ios::in | ios::out);

		if (!file)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����";
		}
		else
		{
			string* buf;
			buf = new string[fileSize];
			unsigned int i{};

			while ((i + 1) != fileSize)
			{
				string copy;
				file >> copy;
				if (copy == deletedComputer.id)
				{
					string cache;
					getline(file, cache);
					continue;
				}
				int len = copy.length();
				file.seekg(-len, ios::cur);
				file.seekp(file.tellp(), ios::beg);

				getline(file, buf[i]);
				i++;
			}
			file.close();

			ofstream inFile(path);

			for (int j{}; j < i; j++)
			{
				inFile << buf[j] << endl;
			}

			inFile.close();
			delete[] buf;
		}
	}

	void createOrderFile()
	{
		if (!System::IO::File::Exists("Order.txt"))
		{
			ofstream file(pathOrder);
			file << "";
			file.close();
		}
	}

	void addOrder(string _name, string _surname, string _idComputer)
	{
		ifstream readFile(pathOrder, ios::in);
		ofstream inFile(pathOrder, ios::app);

		int maxId = 0;

		while (true)
		{
			if (readFile.eof())
			{
				break;
			}

			readFile >> maxId;
			string name, surname;
			int idComputer;
			readFile >> surname;
			readFile >> name;
			readFile >> idComputer;
		}

		if (!inFile)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����" << endl;
		}
		else
		{
			inFile << maxId + 1 << ' ';
			inFile << _surname << ' ';
			inFile << _name << ' ';
			inFile << _idComputer << endl;
		}

		readFile.close();
		inFile.close();
	}

	string* searchingOrder(string _surname, string _name)
	{
		ifstream fromFile(pathOrder);

		if (!fromFile)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����" << endl;
		}
		else
		{
			struct Orders
			{
				int id;
				string name;
				string surname;
				string idComputers;
			} orderComputer{};
			unsigned int k = 0;
			while (true)
			{
				fromFile >> orderComputer.id;
				fromFile >> orderComputer.surname;
				fromFile >> orderComputer.name;
				fromFile >> orderComputer.idComputers;
				if (fromFile.eof())
				{
					break;
				}
				if ((orderComputer.name == _name) && (orderComputer.surname == _surname))
				{
					k++;
				}
			}
			fromFile.close();
			fromFile.open(pathOrder);
			string* str = new string[k + 1];
			str[0] = to_string(k);
			k = 1;
			while (true)
			{
				fromFile >> orderComputer.id;
				fromFile >> orderComputer.surname;
				fromFile >> orderComputer.name;
				fromFile >> orderComputer.idComputers;

				if (fromFile.eof())
				{
					break;
				}

				if ((orderComputer.name == _name) && (orderComputer.surname == _surname))
				{
					str[k] = to_string(orderComputer.id) + ' ' + orderComputer.name + ' '
						+ orderComputer.surname + ' ' + orderComputer.idComputers;
					k++;
				}
			}
			fromFile.close();
			return str;
		}
	}

	void deleteOrder(int id)
	{
		ifstream fromFile(pathOrder);
		ofstream inFile(copyPath);
		if (!fromFile)
		{
			cout << "���� �� ������. ��������, ���������� ������� ����" << endl;
		}
		else
		{
			struct Orders
			{
				int id;
				string name;
				string surname;
				int idComputers;
			} orderComputer{};

			while (true)
			{
				fromFile >> orderComputer.id;
				fromFile >> orderComputer.name;
				fromFile >> orderComputer.surname;
				fromFile >> orderComputer.idComputers;

				if (fromFile.eof())
				{
					break;
				}

				if (orderComputer.id != id)
				{
					inFile << orderComputer.id << ' ';
					inFile << orderComputer.name << ' ';
					inFile << orderComputer.surname << ' ';
					inFile << orderComputer.idComputers << endl;
				}
			}

			inFile.close();
			fromFile.close();

			remove("Order.txt");
			rename("Order1.txt", "Order.txt");
		}
	}


};